# Web_mining_cnki_final
这里是我本学期数据挖掘课程的期末项目作品

## 项目要求
1、提交ipynb档或者可执行.py项目文件  
2、有较好的文档描述和数据描述（包含数据目标和数据结果描述）  
3、主要目标：可对CNKI PDF文件进行依次下载，解决中间处理问题（并做描述）  
4、次要目标：数据分析（关键词替换）——数据可视化（VOSviewer--keywords_co-occurrence）  
5、将作业上传至gitee/github，作为数据挖掘项目作品  

## 一、文档描述

### 1.1 运作流程
- **ipynb档**  
[web_mining_cnki_final](https://gitee.com/dont_have_time/web_mining_cnki_final/blob/master/web_cnki_final.ipynb)（文档内附有注释）  
- 1、完成运用selenium在知网上以‘新媒体’、‘心理’及‘心理学’3个关键词进行C刊领域的专业检索的基本步骤后，首先运用[Python正则表达re模块的findall()](https://www.cnblogs.com/yyds/p/6953348.html)抓取了页面重要信息如文章的标题、作者、详细页链接及pdf下载链接，并储存到excel方便后续使用；  
- 2、接着为解决下载文章过程中可能会遇到的验证码问题，调用、测试了[‘图鉴’图片识别API](http://www.ttshitu.com/docs/python.html#pageTitle)的精准度，由于直接获取验证码网页链接的话图片会变动，所以运用[driver.save_screenshot()](https://www.jianshu.com/p/a538e5cd4a35)的功能把含有验证码的整个页面截图保存，并根据自身电脑屏幕尺寸[对准验证码位置](https://blog.csdn.net/qq_42293590/article/details/96482141)再次截图保存，好方便api对验证码的识别；  
- 3、完成以上步骤之后，直接循环遍历刚刚保存到excel里的pdf下载链接，运用[python try/except语句](https://blog.csdn.net/m0_37822685/article/details/80259402)加入封装好的api图像识别函数；  
- 4、同时运用[os模块](https://www.runoob.com/python3/python3-os-file-methods.html)--len[（os.listdir()）](https://www.runoob.com/python/os-listdir.html)的方法来设置一个判断逻辑，若运行下载链接后再次len得出的结果跟之前一样，就代表下载失败，于是print第X篇文章下载失败，否则print下载成功；  
- 5、完成pdf文件下载后，通过翻页分两次批量导出refworks文件，为后面的VOSviewer数据可视化做准备。

## 二、数据描述

### 2.1 数据目标
使用python+selenium技术手段，基于兴趣在中国知网CNKI上选择‘新媒体’、‘心理’及‘心理学’3个关键词进行C刊领域的专业检索，并实现页面重要信息的抓取、PDF文件依次下载、refworks文件导出、解决中间逻辑问题、进行关键词替换的数据可视化等。

### 2.2 数据结果
- **页面重要信息的抓取**   
[cnki_webmining.xlsx](https://gitee.com/dont_have_time/web_mining_cnki_final/blob/master/cnki_webmining.xlsx)  

- **PDF文件依次下载**  
[pdf.zip](https://gitee.com/dont_have_time/web_mining_cnki_final/blob/master/pdf.zip)  

- **refworks文件导出**  
[refworks文件](https://gitee.com/dont_have_time/web_mining_cnki_final/tree/master/refworks)  


### 2.3 数据分析
- **数据分析（关键词替换）——数据可视化**
![](VOSviewer.png/关键词替换_数据可视化.png)
对refworks文件进行了keywords_co-occurrence的数据可视化，突出展示了主要关键词‘心理学’与其他关键词的关联，同时展示了其他关键词之间的关联
![](VOSviewer.png/关键词替换_数据可视化1.png)
进行了一些中英重复关键词的替换，使数据关系图更简洁明了
## 个人感想
整个过程请教了很多同学，不止是这次项目，之前的课后作业都是，真的非常超级很感谢各位同学的帮助。花了时间尽力去理解和完成，虽然不够厉害完善也还是希望可以给到需要的同学一些参考，但请不要再整个ipynb连注释都一字不改直接搬走了，我真的很...伤...心.............